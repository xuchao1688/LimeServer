import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.bcel.internal.generic.BREAKPOINT;
import com.sun.org.apache.xpath.internal.operations.Mod;
import entity.*;

/**
 * Created by 12097 on 2017/6/6.
 */
public class Main {
    public static void main(String args[]) {
        DBAccess.getDBA();                   //connect to MySQL
        try {
            ServerSocket serversocket = new ServerSocket(2333);
            final int userScale = 1000;
            ObjectOutputStream ooslist[] = new ObjectOutputStream[userScale];
            ObjectInputStream oislist[] = new ObjectInputStream[userScale];   //manage all connections
            while (true) {
                final Socket clientsocket = serversocket.accept();         //wait for client to connect
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int id = -1;
                        try {
                            OutputStream os = clientsocket.getOutputStream();
                            InputStream is = clientsocket.getInputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(os);
                            ObjectInputStream ois = new ObjectInputStream(is);
                            RequestObject reqObj = null;
                            ResponseObject resObj = null;
                            while (true) {
                                reqObj = (RequestObject) ois.readObject();
                                if(reqObj.getReqType() != RequestObject.REQ_HEARTBEAT)
                                    System.out.println(reqObj);
                                switch (reqObj.getReqType()) {              //deal with different request types
                                    case RequestObject.REQ_HEARTBEAT:
                                        id = (int)reqObj.getReqBody();
                                        if(id >= 0){
                                            DBAccess.getDBA().execute("UPDATE users SET online=1 WHERE uid="
                                                    +id+";");
                                            ooslist[id] = oos;
                                            oislist[id] = ois;
                                        }
                                        break;
                                    case RequestObject.REQ_REG:
                                        RegInfo regInfo = (RegInfo) reqObj.getReqBody();
                                        ResultSet rs = DBAccess.getDBA().executeQuery("SELECT COUNT(*) FROM users WHERE BINARY account='" +
                                                regInfo.getAccount() + "' LIMIT 1;");
                                        rs.next();
                                        int count = rs.getInt("COUNT(*)");
                                        if (count == 0) {
                                            rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users ORDER BY uid DESC;");
                                            if(rs.next()){
                                                id = rs.getInt("uid") + 1;
                                            }else {
                                                id = 0;
                                            }
                                            DBAccess.getDBA().execute("INSERT INTO users(uid,account,password,online) " +
                                                    "VALUES(" + id + ",'" + regInfo.getAccount() + "','" + regInfo.getPassword() + "',0);");
                                            DBAccess.getDBA().executeUpdate("CREATE TABLE usergroup_" + id + "(" +
                                                    "gid INT(11) NOT NULL PRIMARY KEY," +
                                                    "display INT(11) NOT NULL," +
                                                    "x_location INT(11) NOT NULL," +
                                                    "y_location INT(11) NOT NULL);");
//                                            DBAccess.getDBA().execute("INSERT INTO contact_"+id+"(uid,account,online) " +
//                                                    "VALUES(" + 21 + ",'" + "luwenjie" + "',"  + 0 + ");");
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("success"));
                                        } else {
                                            resObj = new ResponseObject(ResponseObject.RES_REG, new RegInfoRes("fail"));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_LOG:
                                        LogInfo logInfo = (LogInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT uid FROM users WHERE BINARY password='" +
                                                logInfo.getPassword() + "' AND BINARY account='" +
                                                logInfo.getAccount() + "' LIMIT 1;");
                                        if(rs.next()){
                                            id = rs.getInt("uid");
                                            DBAccess.getDBA().execute("UPDATE users SET online=1 WHERE uid="
                                                    +id+";");
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("success",id));
                                            ooslist[id] = oos;
                                            oislist[id] = ois;
                                        }
                                        else{
                                            resObj = new ResponseObject(ResponseObject.RES_LOG, new LogInfoRes("fail",-1));
                                        }
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_CREATEGROUP:
                                        OnlineTipsValue onlineTips = (OnlineTipsValue) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT gid FROM groups ORDER BY gid DESC;");
                                        if(rs.next()){
                                            id = rs.getInt("gid") + 1;
                                        }else {
                                            id = 0;
                                        }
                                        DBAccess.getDBA().execute("INSERT INTO groups VALUES(" + id + ",'"
                                                + onlineTips.getGroup_name()+"');");
                                        DBAccess.getDBA().executeUpdate("CREATE TABLE groupmember_" + id + "(" +
                                                "memberid INT(11) NOT NULL PRIMARY KEY," +
                                                "role INT(11) NOT NULL);");
                                        DBAccess.getDBA().execute("INSERT INTO groupmember_" + id + " VALUES(" + onlineTips.getOwner_id() + ","
                                                + onlineTips.getRole()+");");
                                        DBAccess.getDBA().executeUpdate("CREATE TABLE groupmessage_" + id + "(" +
                                                "cid INT(11) NOT NULL PRIMARY KEY," +
                                                "date VARCHAR(30) NOT NULL," +
                                                "content VARCHAR(255) NOT NULL," +
                                                "modifierid INT(11) NOT NULL);");
                                        DBAccess.getDBA().execute("INSERT INTO groupmessage_" + id + " VALUES(" + 0
                                                + ",'" + onlineTips.getDate() + "','"
                                                + onlineTips.getContent()+"'," +
                                                onlineTips.getOwner_id() + ");");
                                        DBAccess.getDBA().execute("INSERT INTO usergroup_" + onlineTips.getOwner_id() + " VALUES(" + id + ","
                                                + (onlineTips.getDisplay() ? 1:0) + ","
                                                + onlineTips.getX_location() + ","
                                                + onlineTips.getY_location() + ");");
                                        break;
                                    case RequestObject.REQ_LOADGROUPTIPSLIST:
                                        id = (int) reqObj.getReqBody();
                                        List<OnlineTipsValue> tipsList = new ArrayList<OnlineTipsValue>();
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM usergroup_" + id + ";");
                                        while (rs.next()){
                                            OnlineTipsValue tips = new OnlineTipsValue();
                                            int gid = rs.getInt("gid");
                                            tips.setGroup_id(gid);
                                            tips.setDisplay(rs.getInt("display") == 1);
                                            tips.setX_location(rs.getInt("x_location"));
                                            tips.setY_location(rs.getInt("y_location"));
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT * FROM groupmessage_" + gid + " ORDER BY cid DESC;");
                                            if(rs2.next()){
                                                tips.setDate(rs2.getString("date"));
                                                tips.setContent(rs2.getString("content"));
                                            }
                                            rs2 = DBAccess.getDBA().executeQuery("SELECT groupname FROM groups WHERE gid=" + gid + " LIMIT 1;");
                                            rs2.next();
                                            tips.setGroup_name(rs2.getString("groupname"));
                                            rs2 = DBAccess.getDBA().executeQuery("SELECT memberid FROM groupmember_" + gid
                                                    + " WHERE role=3 LIMIT 1;");
                                            rs2.next();
                                            tips.setOwner_id(rs2.getInt("memberid"));
                                            rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users"
                                                    + " WHERE uid="+ rs2.getInt("memberid") + " LIMIT 1;");
                                            rs2.next();
                                            tips.setOwner_name(rs2.getString("account"));
                                            rs2 = DBAccess.getDBA().executeQuery("SELECT role FROM groupmember_" + gid
                                                    + " WHERE memberid=" + id + " LIMIT 1;");
                                            rs2.next();
                                            tips.setRole(rs2.getInt("role"));
                                            tipsList.add(tips);
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADGROUPTIPSLIST, new LoadGroupListInfoRes(tipsList));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_MODIFYONLINETIPS:
                                        ModifyOnlineTipsInfo modifyOnlineTipsInfo = (ModifyOnlineTipsInfo)reqObj.getReqBody();
                                        onlineTips = modifyOnlineTipsInfo.getTipsValue();
                                        switch (modifyOnlineTipsInfo.getType()){
                                            case ModifyOnlineTipsInfo.MODIFYMYPROFILE:
                                                rs = DBAccess.getDBA().executeQuery("SELECT * FROM groups WHERE gid=" + onlineTips.getGroup_id() + ";");
                                                if(rs.next()){
                                                    DBAccess.getDBA().execute("UPDATE usergroup_" + modifyOnlineTipsInfo.getMid() +" SET "
                                                            + "display=" + onlineTips.getDisplay()
                                                            + ",x_location=" + onlineTips.getX_location()
                                                            + ",y_location=" + onlineTips.getY_location() + " WHERE gid=" + onlineTips.getGroup_id() + ";");
                                                }
                                                break;
                                            case ModifyOnlineTipsInfo.MODIFYTIPCONTENT:
                                                ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT cid FROM groupmessage_" + onlineTips.getGroup_id() + " ORDER BY cid DESC;");
                                                if(rs2.next()){
                                                    id = rs2.getInt("cid") + 1;
                                                }else {
                                                    id = 0;
                                                }
                                                DBAccess.getDBA().execute("INSERT INTO groupmessage_" + onlineTips.getGroup_id() + " VALUES(" + id
                                                        + ",'" + onlineTips.getDate() + "','"
                                                        + onlineTips.getContent()+"'," +
                                                        modifyOnlineTipsInfo.getMid() + ");");
                                                rs2 = DBAccess.getDBA().executeQuery("SELECT * FROM groupmember_" + onlineTips.getGroup_id() + ";");
                                                while(rs2.next()){
                                                    ResultSet rs3 = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid=" + rs2.getInt("memberid") + " LIMIT 1;");
                                                    rs3.next();
                                                    if(rs3.getInt("online")!=0){
                                                        onlineTips.setRole(rs2.getInt("role"));
                                                        resObj = new ResponseObject(ResponseObject.RES_MODIFYONLINETIPS, new ModifyOnlineTipsInfoRes(onlineTips));
                                                        ooslist[rs2.getInt("memberid")].writeObject(resObj);
                                                    }
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                        break;
                                    case RequestObject.REQ_ADDNEWGROUPMEMBER:
                                        NewGroupMemberInfo newGroupMemberInfo = (NewGroupMemberInfo) reqObj.getReqBody();
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM users WHERE BINARY account='"
                                                + newGroupMemberInfo.getMember_name() + "' LIMIT 1;");
                                        if(rs.next()){
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT * FROM usergroup_" + rs.getInt("uid")
                                                    + " WHERE gid=" + newGroupMemberInfo.getGroup_id() + " LIMIT 1;");
                                            if(rs2.next()){
                                                resObj = new ResponseObject(ResponseObject.RES_ADDNEWGROUPMEMBER,"duplicate");
                                                oos.writeObject(resObj);
                                            }else {
                                                DBAccess.getDBA().execute("INSERT INTO usergroup_" + rs.getInt("uid")
                                                        + " VALUES(" + newGroupMemberInfo.getGroup_id() + ",0,0,0);");
                                                DBAccess.getDBA().execute("INSERT INTO groupmember_" + newGroupMemberInfo.getGroup_id()
                                                        + " VALUES(" + rs.getInt("uid") + ",1);");
                                                resObj = new ResponseObject(ResponseObject.RES_ADDNEWGROUPMEMBER,"success");
                                                oos.writeObject(resObj);
                                                if(rs.getInt("online") != 0){
                                                    OnlineTipsValue tips = new OnlineTipsValue();
                                                    int gid = newGroupMemberInfo.getGroup_id();
                                                    tips.setGroup_id(gid);
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT * FROM groupmessage_" + gid + " ORDER BY cid DESC;");
                                                    if(rs2.next()){
                                                        tips.setDate(rs2.getString("date"));
                                                        tips.setContent(rs2.getString("content"));
                                                    }
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT groupname FROM groups WHERE gid=" + gid + " LIMIT 1;");
                                                    rs2.next();
                                                    tips.setGroup_name(rs2.getString("groupname"));
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT memberid FROM groupmember_" + gid
                                                            + " WHERE role=3 LIMIT 1;");
                                                    rs2.next();
                                                    tips.setOwner_id(rs2.getInt("memberid"));
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users"
                                                            + " WHERE uid="+ rs2.getInt("memberid") + " LIMIT 1;");
                                                    rs2.next();
                                                    tips.setOwner_name(rs2.getString("account"));
                                                    rs2 = DBAccess.getDBA().executeQuery("SELECT role FROM groupmember_" + gid
                                                            + " WHERE memberid=" + rs.getInt("uid") + " LIMIT 1;");
                                                    rs2.next();
                                                    tips.setRole(rs2.getInt("role"));
                                                    resObj = new ResponseObject(ResponseObject.RES_BEADDNEWGROUPMEMBER, new ModifyOnlineTipsInfoRes(tips));
                                                    ooslist[rs.getInt("uid")].writeObject(resObj);
                                                }
                                            }
                                        }else {
                                            resObj = new ResponseObject(ResponseObject.RES_ADDNEWGROUPMEMBER,"not exist");
                                            oos.writeObject(resObj);
                                        }
                                        break;
                                    case RequestObject.REQ_LOADGROUPMEMBERLIST:
                                        int gid = (int) reqObj.getReqBody();
                                        List<OnlineTipsMembersValue> membersList = new ArrayList<OnlineTipsMembersValue>();
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM groupmember_" + gid + ";");
                                        while (rs.next()){
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users WHERE uid="
                                                    + rs.getInt("memberid") + " LIMIT 1;");
                                            rs2.next();
                                            OnlineTipsMembersValue membersValue = new OnlineTipsMembersValue();
                                            membersValue.setRole(rs.getInt("role"));
                                            membersValue.setMember_name(rs2.getString("account"));
                                            membersValue.setMember_id(rs.getInt("memberid"));
                                            membersList.add(membersValue);
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADGROUPMEMBERLIST,new LoadGroupMemberListInfoRes(membersList));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_MODIFYGROUPMEMBER:
                                        ModifyGroupMemberInfo modifyGroupMemberInfo = (ModifyGroupMemberInfo)reqObj.getReqBody();
                                        OnlineTipsMembersValue onlineTipsMembersValue = modifyGroupMemberInfo.getOnlineTipsMembersValue();
                                        gid = modifyGroupMemberInfo.getGroup_id();
                                        if(onlineTipsMembersValue.getRole() == 3){
                                            DBAccess.getDBA().execute("UPDATE groupmember_" + gid
                                                    + " SET role=2 WHERE role=3;");
                                            DBAccess.getDBA().execute("UPDATE groupmember_" + gid
                                                    + " SET role=3 WHERE memberid=" + onlineTipsMembersValue.getMember_id() + ";");
                                        }else {
                                            DBAccess.getDBA().execute("UPDATE groupmember_" + gid
                                                    + " SET role=" + onlineTipsMembersValue.getRole() + " WHERE memberid=" + onlineTipsMembersValue.getMember_id() + ";");
                                        }
                                        rs = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid=" + onlineTipsMembersValue.getMember_id() + ";");
                                        rs.next();
                                        if(rs.getInt("online") != 0){
                                            OnlineTipsValue tips = new OnlineTipsValue();
                                            rs = DBAccess.getDBA().executeQuery("SELECT * FROM groupmessage_" + gid + " ORDER BY cid DESC;");
                                            if(rs.next()){
                                                tips.setDate(rs.getString("date"));
                                                tips.setContent(rs.getString("content"));
                                            }
                                            tips.setRole(onlineTipsMembersValue.getRole());
                                            tips.setGroup_id(gid);
                                            resObj = new ResponseObject(ResponseObject.RES_MODIFYONLINETIPS, new ModifyOnlineTipsInfoRes(tips));
                                            ooslist[onlineTipsMembersValue.getMember_id()].writeObject(resObj);
                                        }
                                        break;
                                    case RequestObject.REQ_DELETEGROUPMEMBERS:
                                        modifyGroupMemberInfo = (ModifyGroupMemberInfo)reqObj.getReqBody();
                                        onlineTipsMembersValue = modifyGroupMemberInfo.getOnlineTipsMembersValue();
                                        id = onlineTipsMembersValue.getMember_id();
                                        gid = modifyGroupMemberInfo.getGroup_id();
                                        if(onlineTipsMembersValue.getRole() != 3){
                                            DBAccess.getDBA().execute("DELETE FROM usergroup_" + id + " WHERE gid=" + gid +";");
                                            DBAccess.getDBA().execute("DELETE FROM groupmember_" + gid + " WHERE memberid=" + id + ";");
                                            rs = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid=" + id + " LIMIT 1;");
                                            rs.next();
                                            if(rs.getInt("online") != 0){
                                                resObj = new ResponseObject(ResponseObject.RES_DELETEGROUPMEMBERS,gid);
                                                ooslist[id].writeObject(resObj);
                                            }
                                        }else {
                                            rs = DBAccess.getDBA().executeQuery("SELECT memberid FROM groupmember_" + gid + ";");
                                            while (rs.next()){
                                                ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT online FROM users WHERE uid="
                                                        + rs.getInt("memberid") + " LIMIT 1;");
                                                rs2.next();
                                                DBAccess.getDBA().execute("DELETE FROM usergroup_" + rs.getInt("memberid") + " WHERE gid=" + gid +";");
                                                if(rs2.getInt("online")!=0){
                                                    resObj = new ResponseObject(ResponseObject.RES_DELETEGROUPMEMBERS,gid);
                                                    ooslist[rs.getInt("memberid")].writeObject(resObj);
                                                }
                                            }
                                            DBAccess.getDBA().execute("DELETE FROM groups WHERE gid=" + gid +";");
                                            DBAccess.getDBA().executeUpdate("DROP TABLE groupmember_" + gid);
                                            DBAccess.getDBA().executeUpdate("DROP TABLE groupmessage_" + gid);
                                        }
                                        break;
                                    case RequestObject.REQ_LOADGROUPHISTORYLIST:
                                        gid = (int)reqObj.getReqBody();
                                        List<OnlineTipsHistoryValue> historyList = new ArrayList<OnlineTipsHistoryValue>();
                                        rs = DBAccess.getDBA().executeQuery("SELECT * FROM groupmessage_" + gid + " ORDER BY cid DESC;");
                                        while (rs.next()){
                                            OnlineTipsHistoryValue historyValue = new OnlineTipsHistoryValue();
                                            historyValue.setMessage_id(rs.getInt("cid"));
                                            historyValue.setDate(rs.getString("date"));
                                            historyValue.setContent(rs.getString("content"));
                                            historyValue.setModifier_id(rs.getInt("modifierid"));
                                            ResultSet rs2 = DBAccess.getDBA().executeQuery("SELECT account FROM users WHERE uid="
                                                    + rs.getInt("modifierid") + " LIMIT 1;");
                                            rs2.next();
                                            historyValue.setModifier_name(rs2.getString("account"));
                                            historyList.add(historyValue);
                                        }
                                        resObj = new ResponseObject(ResponseObject.RES_LOADGROUPHISTORYLIST,new LoadGroupHistoryListInfoRes(historyList));
                                        oos.writeObject(resObj);
                                        break;
                                    case RequestObject.REQ_DELETEGROUPHISTORY:
                                        DeleteGroupHistoryInfo deleteGroupHistoryInfo = (DeleteGroupHistoryInfo)reqObj.getReqBody();
                                        gid = deleteGroupHistoryInfo.getGroup_id();
                                        int mid = deleteGroupHistoryInfo.getMessage_id();
                                        DBAccess.getDBA().execute("DELETE FROM groupmessage_" + gid + " WHERE cid=" + mid + ";");
                                        break;
                                    default:
                                        break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                clientsocket.close();
                            } catch (IOException ee) {
                                ee.printStackTrace();
                            }
                            if (id >= 0) {
                                ooslist[id] = null;
                                DBAccess.getDBA().execute("UPDATE users SET online=0 WHERE uid="
                                        + id + ";");
                            }
                        }
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int findSocketID(ObjectOutputStream[] ooslist, ObjectOutputStream oos) {
        for (int i = 0; i < ooslist.length; i++)
            if (ooslist[i] == oos)
                return i;
        return -1;
    }

}
