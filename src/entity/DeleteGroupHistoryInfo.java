package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/21.
 */
public class DeleteGroupHistoryInfo implements Serializable {
    private int message_id;
    private int group_id;
    public DeleteGroupHistoryInfo(int gid,int mid){
        message_id = mid;
        group_id = gid;
    }
    public int getGroup_id(){
        return group_id;
    }
    public int getMessage_id(){
        return message_id;
    }

    @Override
    public String toString(){
        return "group id is " + group_id;
    }
}
