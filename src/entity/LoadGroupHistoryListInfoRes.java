package entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 12097 on 2017/6/21.
 */
public class LoadGroupHistoryListInfoRes implements Serializable {
    private List<OnlineTipsHistoryValue> historyList;
    public LoadGroupHistoryListInfoRes(List<OnlineTipsHistoryValue> list){
        historyList = list;
    }
    public List<OnlineTipsHistoryValue> getHistoryList(){
        return historyList;
    }
    @Override
    public String toString(){
        return "group history list length is " + historyList.size();
    }
}
