package entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 12097 on 2017/6/18.
 */
public class LoadGroupListInfoRes implements Serializable {
    private List<OnlineTipsValue> tipsList;
    public LoadGroupListInfoRes(List<OnlineTipsValue> list){
        tipsList = list;
    }
    public List<OnlineTipsValue> getTipsList(){
        return tipsList;
    }
    @Override
    public String toString(){
        return "group list length is " + tipsList.size();
    }
}
