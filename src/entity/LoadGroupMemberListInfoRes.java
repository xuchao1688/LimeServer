package entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 12097 on 2017/6/20.
 */
public class LoadGroupMemberListInfoRes implements Serializable {
    private List<OnlineTipsMembersValue> membersList;
    public LoadGroupMemberListInfoRes(List<OnlineTipsMembersValue> list){
        membersList = list;
    }
    public List<OnlineTipsMembersValue> getMembersList(){
        return membersList;
    }
    @Override
    public String toString(){
        return "group member list length is " + membersList.size();
    }
}
