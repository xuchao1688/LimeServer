package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/21.
 */
public class ModifyGroupMemberInfo implements Serializable {
    private OnlineTipsMembersValue onlineTipsMembersValue;
    private int group_id;
    public ModifyGroupMemberInfo(int id,OnlineTipsMembersValue value){
        onlineTipsMembersValue = value;
        group_id = id;
    }
    public int getGroup_id(){
        return group_id;
    }
    public OnlineTipsMembersValue getOnlineTipsMembersValue(){
        return onlineTipsMembersValue;
    }
    @Override
    public String toString(){
        return "group id is " + group_id;
    }
}
