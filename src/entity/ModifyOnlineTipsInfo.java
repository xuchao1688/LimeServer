package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/19.
 */
public class ModifyOnlineTipsInfo implements Serializable {
    public static final int MODIFYMYPROFILE = 1;
    public static final int MODIFYTIPCONTENT = 2;
    private int type;
    private OnlineTipsValue tipsValue;
    private int mid;
    public ModifyOnlineTipsInfo(OnlineTipsValue tips, int ty, int id){
        tipsValue = tips;
        type = ty;
        mid = id;
    }
    public OnlineTipsValue getTipsValue(){
        return tipsValue;
    }
    public int getMid(){
        return mid;
    }
    public int getType(){
        return type;
    }
    @Override
    public String toString(){
        return "group tips userid is " + mid;
    }
}
