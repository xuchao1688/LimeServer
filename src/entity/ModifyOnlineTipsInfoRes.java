package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/19.
 */
public class ModifyOnlineTipsInfoRes implements Serializable {
    private OnlineTipsValue tipsValue;
    public ModifyOnlineTipsInfoRes(OnlineTipsValue tips){
        tipsValue = tips;
    }
    public OnlineTipsValue getTipsValue(){
        return tipsValue;
    }
    @Override
    public String toString(){
        return "group tips id is " + tipsValue.getGroup_id();
    }
}
