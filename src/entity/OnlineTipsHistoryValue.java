package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/21.
 */
public class OnlineTipsHistoryValue implements Serializable {
    private int message_id = -1;
    private String date = "";
    private String content = "";
    private int modifier_id = -1;
    private String modifier_name = "";
    public OnlineTipsHistoryValue(){}
    public void setMessage_id(int mid){
        message_id = mid;
    }
    public int getMessage_id(){
        return message_id;
    }
    public void setDate(String dt){
        date = dt;
    }
    public String getDate(){
        return date;
    }
    public void setContent(String cont){
        content = cont;
    }
    public String getContent(){
        return content;
    }
    public void setModifier_id(int mid){
        modifier_id = mid;
    }
    public int getModifier_id(){
        return modifier_id;
    }
    public void setModifier_name(String name){
        modifier_name = name;
    }
    public String getModifier_name(){
        return modifier_name;
    }

    @Override
    public String toString(){
        return "it's a group message.";
    }
}
