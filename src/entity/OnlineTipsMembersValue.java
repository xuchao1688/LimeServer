package entity;

import javafx.beans.property.StringProperty;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/20.
 */
public class OnlineTipsMembersValue implements Serializable{
    private int member_id = -1;
    private String member_name = "";
    private int role = 0;

    public OnlineTipsMembersValue(){
    }
    public void setMember_id(int id){
        member_id =id;
    }
    public int getMember_id(){
        return member_id;
    }
    public void setMember_name(String name){
        member_name = name;
    }
    public String getMember_name(){
        return member_name;
    }
    public void setRole(int ro){
        role = ro;
    }
    public int getRole(){
        return role;
    }

    @Override
    public String toString(){
        return "it's a group member.";
    }
}
