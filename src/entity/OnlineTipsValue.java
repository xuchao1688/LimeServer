package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/19.
 */
public class OnlineTipsValue implements Serializable {
    private int group_id;
    private String group_name = "";
    private String owner_name = "";
    private int owner_id;
    private String date = "";
    private String content = "";
    private boolean display = false;
    private int x_location ;
    private int y_location ;
    private int role;  //3: Owner 2:Administrator 1:normal
    public void setGroup_id(int id){
        group_id = id;
    }
    public int getGroup_id(){
        return group_id;
    }
    public void setGroup_name(String name){
        group_name = name;
    }
    public String getGroup_name(){
        return group_name;
    }
    public void setOwner_name(String name){
        owner_name = name;
    }
    public String getOwner_name(){
        return owner_name;
    }
    public void setOwner_id(int id){
        owner_id = id;
    }
    public int getOwner_id(){
        return owner_id;
    }
    public final void setDate(String value){
        date = value;
    }
    public String getDate(){
        return date;
    }
    public final void setContent(String value){
        if(content == null){
            content = "123";
        }
        content = value;
    }
    public String getContent(){
        return content;
    }
    public final void setDisplay(boolean value){
        display = value;
    }
    public boolean getDisplay(){
        return display;
    }
    public final void setX_location(int x){
        x_location = x;
    }
    public int getX_location(){
        return  x_location;
    }
    public final void setY_location(int y){
        y_location = y;
    }
    public int getY_location(){
        return  y_location;
    }
    public final void setRole(int y){
        role = y;
    }
    public int getRole(){
        return  role;
    }
    @Override
    public String toString(){
        return "it's a group tip.";
    }
}
