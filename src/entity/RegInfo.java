package entity;
import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/2.
 */
public class RegInfo implements Serializable{
    private String account;
    private String password;
    public RegInfo(String name, String pass){
        account = name;
        password = pass;
    }
    @Override
    public String toString(){
        return "account is " + account + ", password is " + password;
    }
    public String getAccount(){
        return account;
    }
    public String getPassword(){
        return password;
    }
}
