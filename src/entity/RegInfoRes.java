package entity;
import java.io.Serializable;

/**
 * Created by Administrator1 on 2016/11/2.
 */
public class RegInfoRes implements Serializable{
    private String message;
    public RegInfoRes(String mess){
        message = mess;
    }
    @Override
    public String toString(){
        return "message is '" + message +"'";
    }
    public String getMessage(){
        return message;
    }
}
