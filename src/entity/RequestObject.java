package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/6.
 */
public class RequestObject implements Serializable{
    public static final int REQ_HEARTBEAT = 0;
    public static final int REQ_REG = 1;
    public static final int REQ_LOG = 2;
    public static final int REQ_CREATEGROUP = 3;
    public static final int REQ_LOADGROUPTIPSLIST = 4;
    public static final int REQ_MODIFYONLINETIPS = 5;
    public static final int REQ_ADDNEWGROUPMEMBER = 6;
    public static final int REQ_LOADGROUPMEMBERLIST = 7;
    public static final int REQ_MODIFYGROUPMEMBER = 8;
    public static final int REQ_DELETEGROUPMEMBERS = 9;
    public static final int REQ_LOADGROUPHISTORYLIST = 10;
    public static final int REQ_DELETEGROUPHISTORY = 11;
    private int reqType;
    private Object reqBody;
    public RequestObject(int reqType,Object reqBody){
        super();
        this.reqType = reqType;
        this.reqBody = reqBody;
    }
    public int getReqType(){
        return reqType;
    }
    public Object getReqBody(){
        return reqBody;
    }
    @Override
    public String toString(){
        return "Request Type: " + reqType + " .Request Content: " + reqBody;
    }

}
