package entity;

import java.io.Serializable;

/**
 * Created by 12097 on 2017/6/7.
 */
public class ResponseObject implements Serializable {
    public static final int RES_REG = 1;
    public static final int RES_LOG = 2;
    public static final int RES_LOADGROUPTIPSLIST = 3;
    public static final int RES_MODIFYONLINETIPS = 4;
    public static final int RES_ADDNEWGROUPMEMBER = 5;
    public static final int RES_LOADGROUPMEMBERLIST = 6;
    public static final int RES_BEADDNEWGROUPMEMBER = 7;
    public static final int RES_DELETEGROUPMEMBERS = 8;
    public static final int RES_LOADGROUPHISTORYLIST = 9;
    private int resType;
    private Object resBody;
    public ResponseObject(int resType,Object resBody){
        super();
        this.resType = resType;
        this.resBody = resBody;
    }
    public int getResType(){
        return resType;
    }
    public Object getResBody(){
        return resBody;
    }
    @Override
    public String toString(){
        return "Response Type: " + resType + " .Response Content: " + resBody;
    }
}
